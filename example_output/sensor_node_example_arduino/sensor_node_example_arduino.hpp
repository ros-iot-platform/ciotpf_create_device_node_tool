#include <ros2arduino.h>
#include <WiFi.h>
#include <WiFiClient.h>

void temperature_publish(std_msgs::Float32* msg, void* arg);
void light_status_publish(std_msgs::Bool* msg, void* arg);
void light_subscriber_callback(std_msgs::Bool* msg, void* arg);

    
class TestESP32DeviceNode: public ros2::Node {
  static constexpr char* NODE_NAME = "test_esp32_device_nodeaaa";
  static constexpr int TEMPERATURE_PUBLISH_RATE = 500;
  static constexpr int TLIGHT_STATUS_PUBLISH_RATE = 500;

  public:
    ros2::Publisher<std_msgs::Float32>* temperature_publisher = nullptr;
    ros2::Publisher<std_msgs::Bool>* light_status_publisher = nullptr;
    ros2::Subscriber<std_msgs::Bool>* light_subscriber = nullptr;
     
    TestESP32DeviceNode(): Node(NODE_NAME) {
      temperature_publisher = this->createPublisher<std_msgs::Float32>("ciotpf/device/test_esp32_device_node/sensor/temperature");
      this->createWallTimer(TEMPERATURE_PUBLISH_RATE, (ros2::CallbackFunc)temperature_publish, nullptr, temperature_publisher);
      
      light_status_publisher = this->createPublisher<std_msgs::Bool>("ciotpf/device/test_esp32_device_node/sensor/light_status");
      this->createWallTimer(TLIGHT_STATUS_PUBLISH_RATE, (ros2::CallbackFunc)light_status_publish, nullptr, light_status_publisher);

      this->createSubscriber<std_msgs::Bool>("ciotpf/device/test_esp32_device_node/actuator/light", (ros2::CallbackFunc)light_subscriber_callback, nullptr);
    }
    
};

TestESP32DeviceNode* node;
void main_loop();

void MainThread(void *pvParameters){
  while(1){
    main_loop();
  }
}

void loop() 
{
  ros2::spin(node);
}
