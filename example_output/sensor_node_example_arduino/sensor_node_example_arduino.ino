#include "sensor_node_example_arduino.hpp"

#define SSID       "aterm-154cb3-g"
#define SSID_PW    "4b2854c0731d7"
#define AGENT_IP   "192.168.10.102"
#define AGENT_PORT 2018

WiFiUDP udp;
void setup() 
{
  Serial.begin(9600);
  WiFi.begin(SSID, SSID_PW);
  while(WiFi.status() != WL_CONNECTED);
  ros2::init(&udp, AGENT_IP, AGENT_PORT);
  node = new TestESP32DeviceNode();
  xTaskCreatePinnedToCore(MainThread, "MainTheread", 1024*16, NULL, 2, nullptr, 1);

}


void main_loop() {
  Serial.println("AA");
  delay(1000);
}


//温度センサの値
//500msecごとに実行される
//例: msg->data = 10.1
void temperature_publish(std_msgs::Float32* msg, void* arg){
  msg->data = 10.1;
}


//照明の状態。
//500msecごとに実行される
//例: msg->data = false
void light_status_publish(std_msgs::Bool* msg, void* arg){
  msg->data = false;
}

//照明のオンオフ
void light_subscriber_callback(std_msgs::Bool* msg, void* arg) {
 Serial.println("light CALLEDDD");
}
