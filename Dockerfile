FROM tiryoh/ros2:dashing

RUN sudo apt update
RUN sudo apt upgrade -y
RUN sudo apt install -y python3-pip
RUN pip3 install pyyaml stringcase

RUN sudo mkdir /opt/ciotpf
RUN sudo git clone https://gitlab.com/ros-iot-platform/ciotpf_ros2_utils.git /opt/ciotpf/ciotpf_ros2_utils

RUN echo 'export PYTHONPATH="/opt/ciotpf:$PYTHONPATH"' >> ~/.bashrc

#これほんとに必要？
RUN echo 'export PYTHONPATH="/home/ubuntu/workspace:$PYTHONPATH"' >> ~/.bashrc

#これでROS2workspaceにパスを通す
RUN echo 'source /home/ubuntu/workspace/ros2_workspace/install/setup.bash' >> ~/.bashrc