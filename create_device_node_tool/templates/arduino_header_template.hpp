#include <ros2arduino.h>
#include <WiFi.h>
#include <WiFiClient.h>

--SENSOR_MESSAGE_GENERATION_PROTOTYPE_DEF--
--ACTUATUATOR_CALLBACK_PROTOTYP_DEF--
    
class --DEVICE_NAME_PASCALCASE--DeviceNode: public ros2::Node {
  static constexpr char* NODE_NAME = "--DEVICE_NAME_SNAKECASE--_device_node";
  --SENSOR_PUBLISH_RATE_DEF--

  public:
    --SENSOR_PUBLISHER_DEF--
    --ACTUATOR_SUBSCRIBER_DEF--
     
    --DEVICE_NAME_PASCALCASE--DeviceNode(): Node(NODE_NAME) {
      --SENSOR_PUBLISHER_INIT--
      --ACTUATOR_SUBSCRIBER_INIT--    
    }
    
};

--DEVICE_NAME_PASCALCASE--DeviceNode* node;
void main_loop();

void MainThread(void *pvParameters){
  while(1){
    main_loop();
  }
}

void loop() 
{
  ros2::spin(node);
}
