import yaml
import sys
import pathlib
import os
import argparse 
import functools

from typing import Dict, TypeVar
from yaml import YAMLObject

import stringcase
from CiotpfDeviceNodeConfig import CiotpfDeviceNodeConfig, CiotpfActuatorConfig, CiotpfSensorConfig


#ヘッダファイルの生成
#{device_name_snake_case}_device_node_arduino" + ".hpp"
def create_header_file(template: str, device_node_config:CiotpfDeviceNodeConfig) -> str:
    device_name_pascal_case = stringcase.pascalcase(device_node_config.deviceName)
    device_name_snake_case = stringcase.snakecase(device_node_config.deviceName)

    SENSOR_MESSAGE_GENERATION_PROTOTYPE_DEF = ""
    ACTUATUATOR_CALLBACK_PROTOTYP_DEF = ""
    DEVICE_NAME_PASCALCASE = device_name_pascal_case
    DEVICE_NAME_SNAKECASE = device_name_snake_case
    SENSOR_PUBLISH_RATE_DEF = ""
    SENSOR_PUBLISHER_DEF = ""
    ACTUATOR_SUBSCRIBER_DEF = ""
    SENSOR_PUBLISHER_INIT = ""
    ACTUATOR_SUBSCRIBER_INIT = ""


    for sensor_config in device_node_config.sensor_list:
        SENSOR_MESSAGE_GENERATION_PROTOTYPE_DEF +=  "\n" + sensor_config.arduino_code_geneartion_message_generation_prototype_def()
        SENSOR_PUBLISH_RATE_DEF += "\n  " + sensor_config.arduino_code_generation_publish_rate_def()
        SENSOR_PUBLISHER_DEF += "\n    " + sensor_config.arduino_code_generation_publisher_def()

        _rows = sensor_config.arduino_code_generation_publisher_init(device_node_config.deviceName)
        for _row in _rows:
            SENSOR_PUBLISHER_INIT += "\n      " + _row

    
    for actuator_config in device_node_config.actuator_list:
        ACTUATUATOR_CALLBACK_PROTOTYP_DEF += "\n" + actuator_config.arduino_code_geneartion_callback_prototype_def() 
        ACTUATOR_SUBSCRIBER_DEF += "\n    " + actuator_config.arduino_code_generation_subscriber_def()
        ACTUATOR_SUBSCRIBER_INIT += "\n      " + actuator_config.arduino_code_generation_subscriber_init(device_node_config.deviceName)


    template = template.replace("--SENSOR_MESSAGE_GENERATION_PROTOTYPE_DEF--", SENSOR_MESSAGE_GENERATION_PROTOTYPE_DEF)
    template = template.replace("--ACTUATUATOR_CALLBACK_PROTOTYP_DEF--", ACTUATUATOR_CALLBACK_PROTOTYP_DEF)
    template = template.replace("--DEVICE_NAME_PASCALCASE--", DEVICE_NAME_PASCALCASE)
    template = template.replace("--DEVICE_NAME_SNAKECASE--", DEVICE_NAME_SNAKECASE)
    template = template.replace("--SENSOR_PUBLISH_RATE_DEF--", SENSOR_PUBLISH_RATE_DEF)
    template = template.replace("--SENSOR_PUBLISHER_DEF--", SENSOR_PUBLISHER_DEF)
    template = template.replace("--ACTUATOR_SUBSCRIBER_DEF--", ACTUATOR_SUBSCRIBER_DEF)
    template = template.replace("--SENSOR_PUBLISHER_INIT--", SENSOR_PUBLISHER_INIT)
    template = template.replace("--ACTUATOR_SUBSCRIBER_INIT--", ACTUATOR_SUBSCRIBER_INIT)

    return template



#{device_name_snake_case}_device_node_arduino" + ".ino"
def create_file(template: str, device_node_config:CiotpfDeviceNodeConfig) -> str:
    device_name_pascal_case = stringcase.pascalcase(device_node_config.deviceName)
    device_name_snake_case = stringcase.snakecase(device_node_config.deviceName)


    SENSOR_MESSAGE_GENERATION_IMPL = ""
    ACTUATUATOR_CALLBACK_IMPL = ""
    DEVICE_NAME_SNAKECASE = device_name_snake_case
    DEVICE_NAME_PASCALCASE = device_name_pascal_case

    for sensor_config in device_node_config.sensor_list:
        SENSOR_MESSAGE_GENERATION_IMPL +=  "\n\n" + sensor_config.arduino_code_generation_message_generation_impl()

    
    for actuator_config in device_node_config.actuator_list:
        ACTUATUATOR_CALLBACK_IMPL += "\n" + actuator_config.arduino_code_generation_callback_impl() 

    template = template.replace("--SENSOR_MESSAGE_GENERATION_IMPL--", SENSOR_MESSAGE_GENERATION_IMPL)
    template = template.replace("--ACTUATUATOR_CALLBACK_IMPL--", ACTUATUATOR_CALLBACK_IMPL)
    template = template.replace("--DEVICE_NAME_SNAKECASE--", DEVICE_NAME_SNAKECASE)
    template = template.replace("--DEVICE_NAME_PASCALCASE--", DEVICE_NAME_PASCALCASE)

    return template 


def main(config_file_path: str, output_path: str):
    config_dict:Dict = {} 
    header_template: str = ""
    template: str = ""

    try:
        with open(config_file_path) as file:
            config_dict = yaml.safe_load(file)
            
    except Exception as e:
        print(e)
        print('Exception occurred while loading YAML...', file=sys.stderr)
        return

    try:
        with open(os.path.dirname(__file__)+"/templates/arduino_header_template.hpp") as file:
            header_template = file.read()
            
    except Exception as e:
        print(e)
        print('Exception occurred while header template file', file=sys.stderr)
        return

    try:
        with open(os.path.dirname(__file__)+"/templates/arduino_template.ino") as file:
            template = file.read()
            
    except Exception as e:
        print(e)
        print('Exception occurred while template file', file=sys.stderr)
        return
    ######

    device_node_config:CiotpfDeviceNodeConfig = CiotpfDeviceNodeConfig(config_dict)
    header_result = create_header_file(header_template, device_node_config)
    result = create_file(template, device_node_config)
    

    ######
    device_name_snake_case = stringcase.snakecase(device_node_config.deviceName)

    dir_path = output_path + "/" + device_name_snake_case + "_device_node_arduino"
    try:
        os.mkdir(dir_path)
    except Exception as e:
        print(e)
        return

    headr_file_path = dir_path +"/"+device_name_snake_case + "_device_node_arduino" + ".hpp"
    file_path = dir_path +"/"+device_name_snake_case + "_device_node_arduino" + ".ino"

    try:
        with open(headr_file_path, mode="x") as f:
            f.write(header_result)
        with open(file_path, mode="x") as f:
            f.write(result)
        
    except Exception as e:
        print(e)
        print("file already exists", file=sys.stderr)
        return

#実行例. pathは相対パスでも絶対パスでもOK.　相対パスのときは、実行した位置からの相対パス。
#python3 create_device_node_tool/main.py ./spec_example.cpfc.yaml ./test_output/
if __name__ == "__main__":
    parser = argparse.ArgumentParser()  
    parser.add_argument('input_file', help='仕様書ファイル')   
    parser.add_argument('output_file_dir', help='出力のディレクトリ')

    args = parser.parse_args()

    input_file = args.input_file
    output_file_dir = args.output_file_dir 

    input_file_path = pathlib.Path(input_file)
    output_file_dir_path = pathlib.Path(output_file_dir)

    if input_file_path.is_dir():
        raise Exception("ファイルを指定して！")
    
    if output_file_dir_path.is_file():
        raise Exception("出力はディレクトリを指定して！")

    if not input_file_path.is_absolute():
        input_file_path = input_file_path.resolve()
    if not output_file_dir_path.is_absolute():
        output_file_dir_path = output_file_dir_path.resolve()


    input_file_path = str(input_file_path)
    output_file_dir_path = str(output_file_dir_path)

    main(input_file_path, output_file_dir_path)
    #main("../spec_example2.cpfc.yaml", "../test_output/")