from re import template
from typing import Dict, List, Tuple
import stringcase

allowed_types = ["Bool", "Float", "Int", "String"]
allowed_types_in_ros = {"Bool": "Bool", "Float": "Float32", "Int":"Int32", "String": "String"}
allowed_types_in_arduino = {"Bool": "bool", "Float": "float", "Int":"int", "String": "String"}

def check_basic(base_dict: Dict, necessary_attibutes: Dict):
    for attribute in necessary_attibutes:

        if attribute not in base_dict:
            raise Exception(str(base_dict) + "\n"+attribute+" is not in config file")

        #型があっているか。Nameならstrだし、Numberならint
        if type(base_dict[attribute]) is not necessary_attibutes[attribute]:
            raise Exception(str(base_dict) + "\n"+attribute+" wrong type")

    if base_dict["Type"] not in allowed_types:
        raise Exception(base_dict["Name"] + ": " + base_dict["Type"] +"Typeにだめなやつが指定されてるよ！")

    if base_dict["TopicName"][0] != "~" and base_dict["TopicName"][0] != "/":
        raise Exception(base_dict["Name"] + "  topic name should start from ~ or /")

class CiotpfSensorConfig:
    necessary_attibutes = {"Name": str, "TopicName": str, "Type": str, "Rate": int, "Comment": str}

    def check_attributes(self, base_dict: Dict) -> None:
        check_basic(base_dict, CiotpfSensorConfig.necessary_attibutes)


    def __init__(self, base_dict: Dict) -> None:
        self.check_attributes(base_dict)

        self.name:str = base_dict["Name"]
        self.topicName:str = base_dict["TopicName"]
        self.topicType:str = base_dict["Type"]
        self.rate:int = base_dict["Rate"]
        self.comment:str = base_dict["Comment"]

    #例:
    #void temperature_publish(std_msgs::Float32* msg, void* arg)
    def _arduino_code_generation_message_generation_def(self) -> str:
        name_snakecase = stringcase.snakecase(self.name)
        type_name_in_ros = allowed_types_in_ros[self.topicType]

        _template = "void %s_publish(std_msgs::%s* msg, void* arg)" % (name_snakecase, type_name_in_ros) 
        return _template
        
    #ヘッダファイルでのpublishする値を取得する関数のプロトタイプ宣言のところ。
    #例: void temperature_publish(std_msgs::Float32* msg, void* arg);
    def arduino_code_geneartion_message_generation_prototype_def(self) -> str:
        _template = "%s;" %(self._arduino_code_generation_message_generation_def())
        return _template

 
    #static constexpr int TLIGHT_STATUS_PUBLISH_RATE = 500;
    def arduino_code_generation_publish_rate_def(self) -> str:
        name_pascal_case = stringcase.pascalcase(self.name)
        _template = "static constexpr int %s_PUBLISH_RATE = %s;" %(name_pascal_case, self.rate)
        return _template
    
    #ros2::Publisher<std_msgs::Float32>* temperature_publisher = nullptr;
    def arduino_code_generation_publisher_def(self) -> str:
        name_snakecase = stringcase.snakecase(self.name)
        type_name_in_ros = allowed_types_in_ros[self.topicType]

        _template = "ros2::Publisher<std_msgs::%s>* %s_publisher = nullptr;" %(type_name_in_ros, name_snakecase)
        return _template

    #temperature_publisher = this->createPublisher<std_msgs::Float32>("ciotpf/device/test_esp32_device_node/sensor/temperature");
    #this->createWallTimer(TEMPERATURE_PUBLISH_RATE, (ros2::CallbackFunc)temperature_publish, nullptr, temperature_publisher);
    def arduino_code_generation_publisher_init(self, deviceName: str) -> List[str]:
        device_name_snakecase = stringcase.snakecase(deviceName)
        type_name_in_ros = allowed_types_in_ros[self.topicType]
        name_pascal_case = stringcase.pascalcase(self.name)
        name_snakecase = stringcase.snakecase(self.name)

        _topic_name = "ciotpf/device/%s/sensor/%s" % (device_name_snakecase, name_snakecase)
        if len(_topic_name) > 60:
            raise Exception(_topic_name + "\n topic name is longer than 60. もっと短い名前使って！")

        _template_row1 = '%s_publisher = this->createPublisher<std_msgs::%s>("%s");' % (
                                                                                        name_snakecase,
                                                                                        type_name_in_ros, 
                                                                                        _topic_name)
        _template_row2 = 'this->createWallTimer(%s_PUBLISH_RATE, (ros2::CallbackFunc)%s_publish, nullptr, %s_publisher);' % (
                                                                                                                name_pascal_case,
                                                                                                                name_snakecase,
                                                                                                                name_snakecase)
        return [_template_row1, _template_row2]
    


    #補助用のコメントの生成
    def _arduino_code_generation_message_generation_impl_comment(self) -> str:
        comment = "//%s\n" % (self.comment)
        comment += "//%smsecごとに実行される\n" % (self.rate)

        if self.topicType == "Bool":
            comment += "//msg->data = false \n"
        elif self.topicType == "Float":
            comment += "//msg->data = 10.12 \n"
        elif self.topicType == "Int":
            comment += "//msg->data = 2 \n"
        elif self.topicType == "String":
            comment += ' sprintf(msg->data, "Hello ros2arduino %d", cnt++);'

        return comment

    #例: 
    #//温度センサの値
    #//500msecごとに実行される
    #//例: msg->data = 10.1
    #void temperature_publish(std_msgs::Float32* msg, void* arg){
    #  
    #}
    def arduino_code_generation_message_generation_impl(self) -> str:
        
        _template = self._arduino_code_generation_message_generation_impl_comment()
        _template +=  self._arduino_code_generation_message_generation_def() + "{ \n"
        _template += "\n}"

        return _template

class CiotpfActuatorConfig:
    necessary_attibutes = {"Name": str, "TopicName": str, "Type": str, "Comment": str}

    def check_attributes(self, base_dict: Dict) -> None:
        check_basic(base_dict, CiotpfActuatorConfig.necessary_attibutes)



    def __init__(self, base_dict: Dict) -> None:
        self.check_attributes(base_dict)

        self.name:str = base_dict["Name"]
        self.topicName:str = base_dict["TopicName"]
        self.topicType:str = base_dict["Type"]
        self.comment:str = base_dict["Comment"]

    #例:
    #void light_subscriber_callback(std_msgs::Bool* msg, void* arg)
    def _arduino_code_generation_callback_def(self) -> str:
        name_snakecase = stringcase.snakecase(self.name)
        type_name_in_ros = allowed_types_in_ros[self.topicType]

        _template = "void %s_subscriber_callback(std_msgs::%s* msg, void* arg)" % (name_snakecase, type_name_in_ros) 
        return _template
        

    #ヘッダファイルでcallback関数のプロトタイプ宣言
    #例: void light_subscriber_callback(std_msgs::Bool* msg, void* arg);
    def arduino_code_geneartion_callback_prototype_def(self) -> str:

        _template = self._arduino_code_generation_callback_def() + ";" 
        return _template

    #ros2::Subscriber<std_msgs::Bool>* light_subscriber = nullptr;
    def arduino_code_generation_subscriber_def(self) -> str:
        name_snakecase = stringcase.snakecase(self.name)
        type_name_in_ros = allowed_types_in_ros[self.topicType]

        _template = "ros2::Subscriber<std_msgs::%s>* %s_subscriber = nullptr;" %(type_name_in_ros, name_snakecase)
        return _template

    #this->createSubscriber<std_msgs::Bool>("ciotpf/device/test_esp32_device_node/actuator/light", (ros2::CallbackFunc)light_subscriber_callback, nullptr);
    def arduino_code_generation_subscriber_init(self, deviceName: str) -> str:
        device_name_snakecase = stringcase.snakecase(deviceName)
        type_name_in_ros = allowed_types_in_ros[self.topicType]
        name_snakecase = stringcase.snakecase(self.name)
        
        _topic_name = "ciotpf/device/%s/actuator/%s" % (device_name_snakecase, name_snakecase)
        if len(_topic_name) > 60:
            raise Exception(_topic_name + "\n topic name is longer than 60. もっと短い名前使って！")

        _template = 'this->createSubscriber<std_msgs::%s>("%s", (ros2::CallbackFunc)%s_subscriber_callback, nullptr);' % (
                                                                                                                type_name_in_ros, 
                                                                                                                _topic_name,
                                                                                                                name_snakecase)
      
        return _template


    #補助用のコメントの生成
    def _arduino_code_generation_callback_impl_comment(self) -> str:
        comment = "//%s\n" % (self.comment)

        if self.topicType == "Bool":
            comment += "//bool value = msg->data; \n"
        elif self.topicType == "Float":
            comment += "//float value = msg->data; \n"
        elif self.topicType == "Int":
            comment += "//int value = msg->data; \n"
        elif self.topicType == "String":
            comment += 'String value = msg->data;'

        return comment

    #//照明のオンオフ
    #//bool value = msg->data;
    #void light_subscriber_callback(std_msgs::Bool* msg, void* arg) {
    # 
    #}
    def arduino_code_generation_callback_impl(self) -> str:
        
        _template = self._arduino_code_generation_callback_impl_comment()
        _template +=  self._arduino_code_generation_callback_def() + "{ \n"
        _template += "\n}"

        return _template

class CiotpfDeviceNodeConfig:
    neccesarry_attributes = ["DeviceName", "Sensors", "Actuators", "NodeDescription"]

    def check_attributes(config_dict: Dict) -> None:
        for attribute in CiotpfDeviceNodeConfig.neccesarry_attributes:
            if attribute not in config_dict:
                raise Exception( attribute+" is not in config file")
        

    def __init__(self, config_dict: Dict) -> None:
        CiotpfDeviceNodeConfig.check_attributes(config_dict)

        self.sensor_list: List[CiotpfSensorConfig] = [CiotpfSensorConfig(sensor) for sensor in config_dict["Sensors"]]
        self.actuator_list: List[CiotpfActuatorConfig] = [CiotpfActuatorConfig(actuator) for actuator in config_dict["Actuators"]]
        self.deviceName: str = config_dict["DeviceName"]
        self.nodeDescription: str = config_dict["NodeDescription"]

